# Salesforce Status Mail

The module sends mails when Salesforce connection:

- Fails: Endpoints are not working but they were available before.
- Is back to normal: Endpoints weren't working but now are available.

# Installation

After enabling the module, it can be configured at
Configuration / Salesforce / Status Mail Settings.

It is required to setup the list of emails that will receive
the notification.
