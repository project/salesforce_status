<?php

namespace Drupal\Tests\salesforce_status\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test events are correctly sent when salesforce fails or backs to normal.
 */
class SalesforceStatusMailConfigTest extends KernelTestBase {

  /**
   * Required modules.
   *
   * @var array<string>
   */
  protected static $modules = [
    'salesforce',
    'salesforce_status',
    'salesforce_status_mail',
  ];

  /**
   * Ensures schema / configuration is installed correctly.
   */
  public function testConfigurationIsCorrectlyInstalled() {
    $this->installConfig(['salesforce_status_mail']);
    $mail_recipients = $this->config('salesforce_status_mail.settings')
      ->get('mail_recipients');
    $this->assertIsArray($mail_recipients);
    $this->assertEmpty($mail_recipients);
  }

}
