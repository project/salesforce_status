<?php

namespace Drupal\salesforce_status_mail\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Salesforce Status Mail settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Check e-mails written are correct.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $emailValidator;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    /** @var \Drupal\Component\Utility\EmailValidatorInterface $email_validator */
    $email_validator = $container->get('email.validator');
    $instance->emailValidator = $email_validator;
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'salesforce_status_mail_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['salesforce_status_mail.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['mail_recipients'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Mail recipients'),
      '#description' => $this->t('List of e-mails, separated by break lines, that will receive the status.'),
      '#default_value' => implode("\r\n", $this->config('salesforce_status_mail.settings')->get('mail_recipients') ?? []),
      '#validate' => [
        '::validateEmails',
      ],
      '#required' => TRUE,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateEmails(array &$form, FormStateInterface $form_state) {
    $emails = array_filter(explode("\r\n", $form_state->getValue('mail_recipients')));
    $invalid_emails = [];
    foreach ($emails as $email) {
      if (!$this->emailValidator->isValid($email)) {
        $invalid_emails[] = $email;
      }
    }

    if (!empty($invalid_emails)) {
      $form_state->setError(
        $form['mail_recipients'],
        $this->t('Invalid emails set: @email_list', ['@email_list' => implode(', ', $invalid_emails)])
      );
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('salesforce_status_mail.settings')
      ->set('mail_recipients', explode("\r\n", $form_state->getValue('mail_recipients')))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
