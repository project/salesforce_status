<?php

namespace Drupal\salesforce_status_mail\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\salesforce_status\Event\SalesforceStatusEvent;
use Drupal\salesforce_status\Event\SalesforceStatusEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Sends mails when salesforce connection fails / is back to normal.
 */
class SalesforceStatusMailSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * Mail manager service.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * Used to get the site name.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $systemSiteConfig;

  /**
   * Used to get the mail recipients.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $salesforceStatusMailConfig;

  /**
   * Used to get the mail language.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs a SalesforceStatusMailSubscriber object.
   *
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   Mail manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Configuration factory service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   Language manager service.
   */
  public function __construct(MailManagerInterface $mail_manager, ConfigFactoryInterface $configFactory, LanguageManagerInterface $languageManager) {
    $this->mailManager = $mail_manager;
    $this->systemSiteConfig = $configFactory->get('system.site');
    $this->salesforceStatusMailConfig = $configFactory->get('salesforce_status_mail.settings');
    $this->languageManager = $languageManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      SalesforceStatusEvents::STATUS_FAIL => ['onSalesforceFail'],
      SalesforceStatusEvents::STATUS_BACK_TO_NORMAL => ['onSalesforceBackToNormal'],
    ];
  }

  /**
   * Sends a mail when salesforce connection is failing.
   *
   * @param \Drupal\salesforce_status\Event\SalesforceStatusEvent $event
   *   Event.
   *
   * @SuppressWarnings(PHPMD.UnusedFormalParameter)
   */
  public function onSalesforceFail(SalesforceStatusEvent $event) {
    $this->sendMail(
      $this->t('@site_name: Salesforce connection is not available', ['@site_name' => $this->systemSiteConfig->get('name')]),
      $this->t('Salesforce is unreachable right now, please contact with an administrator as soon as possible.'),
    );
  }

  /**
   * Sends a mail when salesforce connection is back to normal.
   *
   * @param \Drupal\salesforce_status\Event\SalesforceStatusEvent $event
   *   Event.
   *
   * @SuppressWarnings(PHPMD.UnusedFormalParameter)
   */
  public function onSalesforceBackToNormal(SalesforceStatusEvent $event) {
    $this->sendMail(
      $this->t('@site_name - URGENT: Salesforce connection is back to normal', ['@site_name' => $this->systemSiteConfig->get('name')]),
      $this->t('Salesforce connection is back to normal.'),
    );
  }

  /**
   * Sends the mail notifying status has changed.
   *
   * @param string $subject
   *   Mail subject.
   * @param string $body
   *   Mail body.
   */
  protected function sendMail(string $subject, string $body) {
    $params = [
      'subject' => $subject,
      'body' => $body,
    ];

    $mail_recipients = $this->salesforceStatusMailConfig->get('mail_recipients') ?? [];
    if (!empty($mail_recipients)) {
      $to = implode(', ', $mail_recipients);

      $this->mailManager->mail(
        'salesforce_status_mail',
        'salesforce_status_changed',
        $to,
        $this->languageManager->getCurrentLanguage()->getId(),
        $params
      );
    }
  }

}
