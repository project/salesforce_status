<?php

namespace Drupal\salesforce_status_test;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Modifies the salesforce client service.
 */
class SalesforceStatusTestServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $container->getDefinition('salesforce.client')
      ->setClass(TestRestClient::class);
  }

}
