<?php

namespace Drupal\salesforce_status_test;

use Drupal\salesforce\Tests\TestRestClient as OriginalTestRestClient;

/**
 * Allows simulating errors into salesforce.
 *
 * This is done so the status checker receives
 * the signal so we can register when the
 * status is failing / back to normal and test it.
 */
class TestRestClient extends OriginalTestRestClient {

  /**
   * Flag to simulate salesforce is failing.
   *
   * @var bool
   */
  public static bool $simulateError = FALSE;

  /**
   * {@inheritdoc}
   *
   * When flag to simulate errors is set, it will return an exception.
   */
  public function objects(array $conditions = ['updateable' => TRUE], $reset = FALSE) {
    if (self::$simulateError) {
      throw new \Exception('Test');
    }

    return parent::objects($conditions, $reset);
  }

  /**
   * Reset the flag.
   *
   * Used for test setups.
   */
  public static function reset() {
    self::$simulateError = FALSE;
  }

}
