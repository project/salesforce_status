<?php

namespace Drupal\salesforce_status_test\EventSubscriber;

use Drupal\salesforce_status\Event\SalesforceStatusEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Registers events from salesforce status so it can be tested.
 */
class TestEventSubscriber implements EventSubscriberInterface {

  /**
   * Latest event registered, if it was registered.
   *
   * @var string|null
   */
  public static ?string $latestEvent;

  /**
   * Reset latest event so is empty.
   *
   * Used for test setups.
   */
  public static function reset() {
    self::$latestEvent = NULL;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      SalesforceStatusEvents::STATUS_FAIL => ['registerStatusFailEvent'],
      SalesforceStatusEvents::STATUS_BACK_TO_NORMAL => ['registerStatusBackToNormalEvent'],
    ];
  }

  /**
   * Register salesforce started failing.
   */
  public function registerStatusFailEvent() {
    $this->registerEvent(SalesforceStatusEvents::STATUS_FAIL);
  }

  /**
   * Register salesforce is back to normal.
   */
  public function registerStatusBackToNormalEvent() {
    $this->registerEvent(SalesforceStatusEvents::STATUS_BACK_TO_NORMAL);
  }

  /**
   * Register an event.
   *
   * Used by event subscribers.
   *
   * @param string $event
   *   Event.
   */
  protected function registerEvent(string $event) {
    self::$latestEvent = $event;
  }

}
