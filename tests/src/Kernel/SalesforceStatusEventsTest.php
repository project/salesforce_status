<?php

namespace Drupal\Tests\salesforce_status\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\salesforce_status\Event\SalesforceStatusEvents;
use Drupal\salesforce_status_test\EventSubscriber\TestEventSubscriber;
use Drupal\salesforce_status_test\TestRestClient;

/**
 * Test events are correctly sent when salesforce fails or backs to normal.
 */
class SalesforceStatusEventsTest extends KernelTestBase {

  /**
   * Required modules.
   *
   * @var array<string>
   */
  protected static $modules = [
    'salesforce',
    'salesforce_test_rest_client',
    'salesforce_status',
    'salesforce_status_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    TestEventSubscriber::reset();
    TestRestClient::reset();
  }

  /**
   * Test that no events are sent if there are no regressions.
   *
   * The cron is run several times to ensure that after
   * N and N+1 times it does not sends any event.
   */
  public function testNoEventsIfNoRegressions() {
    $this->runCron();
    $this->assertEmpty(TestEventSubscriber::$latestEvent);

    $this->runCron();
    $this->runCron();
    $this->assertEmpty(TestEventSubscriber::$latestEvent);
  }

  /**
   * Test event when salesforce was working and it stops working is sent.
   */
  public function testEventIfFails() {
    $this->runCron();
    TestRestClient::$simulateError = TRUE;
    $this->runCron();
    $this->assertEquals(SalesforceStatusEvents::STATUS_FAIL, TestEventSubscriber::$latestEvent);
  }

  /**
   * Test event when salesforce was not working and is back to normal is sent.
   */
  public function testEventIfBackToNormal() {
    $this->runCron();
    TestRestClient::$simulateError = TRUE;
    $this->runCron();
    TestRestClient::$simulateError = FALSE;
    $this->runCron();
    $this->assertEquals(SalesforceStatusEvents::STATUS_BACK_TO_NORMAL, TestEventSubscriber::$latestEvent);
  }

  /**
   * Run the salesforce cron that sends the events.
   */
  protected function runCron() {
    salesforce_status_cron();
  }

}
