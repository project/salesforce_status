<?php

namespace Drupal\salesforce_status\Event;

use Drupal\salesforce\Event\SalesforceBaseEvent;
use Drupal\salesforce_status\Status;

/**
 * Event object when the salesforce status request fails.
 */
class SalesforceStatusEvent extends SalesforceBaseEvent {

  /**
   * Exception thrown trying to get a basic endpoint.
   *
   * @var \Drupal\salesforce_status\Status
   */
  protected $status;

  /**
   * Constructs the event.
   *
   * @param \Drupal\salesforce_status\Status $status
   *   The current status of salesforce.
   */
  public function __construct(Status $status) {
    $this->status = $status;
  }

  /**
   * Gets the status.
   *
   * @return \Drupal\salesforce_status\Status
   *   Salesforce status.
   */
  public function getStatus() {
    return $this->status;
  }

}
