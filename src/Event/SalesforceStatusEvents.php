<?php

namespace Drupal\salesforce_status\Event;

/**
 * Events sent by salesforce status.
 */
final class SalesforceStatusEvents {

  const STATUS_FAIL = 'salesforce_status.fail';

  const STATUS_BACK_TO_NORMAL = 'salesforce_status.back_to_normal';

}
