<?php

namespace Drupal\salesforce_status\Plugin\SalesforcePushQueueProcessor;

use Drupal\Core\Queue\SuspendQueueException;
use Drupal\salesforce_status\SalesforceStatusManager;

/**
 * Used to create queue processor plugins that manages status.
 */
trait SalesforceStatusQueueProcessorTrait {

  /**
   * Checks salesforce is not available to stop processing.
   *
   * @var \Drupal\salesforce_status\SalesforceStatusManager
   */
  protected $statusManager;

  /**
   * Set the status manager.
   *
   * @param \Drupal\salesforce_status\SalesforceStatusManager $statusManager
   *   Status manager service.
   */
  public function setStatusManager(SalesforceStatusManager $statusManager) {
    $this->statusManager = $statusManager;
  }

  /**
   * {@inheritdoc}
   */
  public function process(array $items) {
    if ($this->statusManager instanceof SalesforceStatusManager && !$this->statusManager->getStatus()->isAvailable()) {
      throw new SuspendQueueException('Salesforce instance not available.');
    }
    parent::process($items);
  }

}
