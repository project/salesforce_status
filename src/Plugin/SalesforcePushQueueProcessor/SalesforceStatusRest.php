<?php

namespace Drupal\salesforce_status\Plugin\SalesforcePushQueueProcessor;

use Drupal\salesforce_push\Plugin\SalesforcePushQueueProcessor\Rest;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Rest queue processor plugin.
 *
 * @Plugin(
 *   id = "salesforce_status_rest",
 *   label = @Translation("Salesforce Status REST Push Queue Processor")
 * )
 */
class SalesforceStatusRest extends Rest {

  use SalesforceStatusQueueProcessorTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    /**  @var \Drupal\salesforce_status\SalesforceStatusManager $status_manager */
    $status_manager = $container->get('salesforce_status.manager');
    $instance->setStatusManager($status_manager);
    return $instance;
  }

}
