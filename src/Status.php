<?php

namespace Drupal\salesforce_status;

/**
 * Salesforce current status.
 */
class Status {

  /**
   * TRUE when is available.
   *
   * @var bool
   */
  protected $status;

  /**
   * Indicates why Salesforce is not available.
   *
   * Its value is empty when Salesforce is available.
   *
   * @var \Exception|null
   */
  protected $exception;

  /**
   * Constructs the status.
   *
   * @param bool $status
   *   Status.
   * @param \Exception|null $exception
   *   The exception.
   */
  public function __construct($status, ?\Exception $exception = NULL) {
    $this->status = $status;
    $this->exception = $exception;
  }

  /**
   * Check if salesforce is available or not.
   *
   * @return bool
   *   TRUE when it is available.
   */
  public function isAvailable() {
    return $this->status;
  }

  /**
   * Gets the exception that makes salesforce not available.
   *
   * @return \Exception|null
   *   The exception, only when salesforce is not available.
   */
  public function getException() {
    return $this->exception;
  }

}
