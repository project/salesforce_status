<?php

namespace Drupal\salesforce_status;

use Drupal\salesforce\Rest\RestClientInterface;

/**
 * Service description.
 */
class SalesforceStatusManager {

  /**
   * The salesforce.client service.
   *
   * @var \Drupal\salesforce\Rest\RestClientInterface
   */
  protected $client;

  /**
   * Constructs a SalesforceStatusManager object.
   *
   * @param \Drupal\salesforce\Rest\RestClientInterface $client
   *   The salesforce.client service.
   */
  public function __construct(RestClientInterface $client) {
    $this->client = $client;
  }

  /**
   * Gets the status with the salesforce connection.
   *
   * To do it, the sobjects endpoint is called, assuming
   * that it must always respond. If it doesn't, connection
   * is not going okay.
   *
   * @return \Drupal\salesforce_status\Status
   *   TRUE when connection is okay.
   */
  public function getStatus() {
    try {
      $this->client->objects(['updateable' => TRUE], TRUE);
      $status = new Status(TRUE);
    }
    catch (\Exception $exception) {
      $status = new Status(FALSE, $exception);
    }
    return $status;
  }

}
