<?php

namespace Drupal\salesforce_status\EventSubscriber;

use Drupal\salesforce_status\Event\SalesforceStatusEvent;
use Drupal\salesforce_status\Event\SalesforceStatusEvents;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Sends mails when salesforce connection fails / is back to normal.
 */
class SalesforceStatusEventSubscriber implements EventSubscriberInterface {

  /**
   * Logs salesforce events.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a SalesforceStatusMailSubscriber object.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The salesforce status logger service.
   */
  public function __construct(LoggerInterface $logger) {
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      SalesforceStatusEvents::STATUS_FAIL => ['onSalesforceFail'],
      SalesforceStatusEvents::STATUS_BACK_TO_NORMAL => ['onSalesforceBackToNormal'],
    ];
  }

  /**
   * Log when salesforce connection is failing.
   *
   * @param \Drupal\salesforce_status\Event\SalesforceStatusEvent $event
   *   Event.
   *
   * @SuppressWarnings(PHPMD.UnusedFormalParameter)
   */
  public function onSalesforceFail(SalesforceStatusEvent $event) {
    $this->logger->error('Salesforce is unreachable right now, please contact with an administrator as soon as possible.');
  }

  /**
   * Log when salesforce connection is back to normal.
   *
   * @param \Drupal\salesforce_status\Event\SalesforceStatusEvent $event
   *   Event.
   *
   * @SuppressWarnings(PHPMD.UnusedFormalParameter)
   */
  public function onSalesforceBackToNormal(SalesforceStatusEvent $event) {
    $this->logger->info('Salesforce connection is back to normal.');
  }

}
