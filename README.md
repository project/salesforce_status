# Salesforce Status

Enhances the features of Salesforce by providing a full status of the Salesforce connection.
Use this module in case sending / receiving data from Salesforce is critical for the site
and it is needed to take urgent actions when the site stops working.

To determine Salesforce is working or not, the module checks a basic
endpoint is working (/sobjects). This method ensures that the oauth token is working / has been refreshed,
plus the endpoint gives a response.

The module provides:

- Events when Salesforce connection changes:
  - From being stable to start failing.
  - From failing to being back to normal.
- A push queue handler that do not send data to Salesforce when endpoints are failing.
- Logs that indicates when the connection is failing and when it is back to normal.
- Status report to indicate Salesforce is not available.

# Event subscribers

It is possible to subscribe to:

- **\Drupal\salesforce_status\Event\SalesforceStatusEvents::STATUS_FAIL** :  When connection changes from being stable
  to not work.
- **\Drupal\salesforce_status\Event\SalesforceStatusEvents::STATUS_BACK_TO_NORMAL** :  When connection changes from
  not working to being back go normal.

Subscribe to these events in case it is needed to perform additional operations.  There are examples
at the salesforce_status_mail submodule.

# Mail notification

For mail notifications, enable and configure the salesforce_status_mail module.
Check its [README.md](/modules/salesforce_status_mail/README.md) for more information.
